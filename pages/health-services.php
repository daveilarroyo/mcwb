<section id="service-banner">
	<div>1</div>
	<div>2</div>
	<div>3</div>
	<div>4</div>
	<div>5</div>
</section>
<section id="services" class="container text-center">
	<p>Our focus is on service <br/> of the highest quality.</p>
	<div id="open-247">
		<span>OPEN</span>
		<div>24/7</div>
		<span>Health Services</span>
	</div>
	<ul>
		<li>Adult and Pediatric ICU</li>
		<li>Ambulance</li>
		<li>Complete Laboratory</li>
		<li>Delivert Room</li>
		<li>Emergency Room</li>
		<li>Normal & Pathologic Nurses</li>
		<li>Operating Room</li>
		<li>Pharmacy</li>
		<li>X-ray & CT Scan</li>
	</ul>
</section>
<section id="schedules" class="container">
	<h3>Schedules</h3>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<table class="table">
				<thead>
					<tr>
						<th>Department</th>
						<th>Weekdays</th>
						<th>Saturday</th>
						<th>Sunday</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>OPD Consultation</td>
						<td>8 AM  - 5PM</td>
						<td>8 AM  - 5PM</td>
						<td>&mdash;</td>
					</tr>
					<tr>
						<td>Holter monitoring</td>
						<td>8 AM  - 5PM</td>
						<td>&mdash;</td>
						<td>&mdash;</td>
					</tr>
					<tr>
						<td>2D Echo</td>
						<td>8 AM  - 3PM</td>
						<td>&mdash;</td>
						<td>&mdash;</td>
					</tr>
					<tr>
						<td>Stress Test</td>
						<td colspan="3">By Appointment</td>
					</tr>
				</tbody>
			</table>
			<p>To reserve an appointment or check availability.</p>
			<div id="book-wrapper" class="text-center">
				<button class="btn btn-primary">Book Online</button>
			</div>
		</div>
	</div>
</section>
<section id="specialists-banner">
	<div>1</div>
	<div>2</div>
	<div>3</div>
	<div>4</div>
	<div>5</div>
</section>
<section id="specialists" class="container">
	<h3>Specialists</h3>
	<div class="contatiner">
		<div class="row">
			<div class="col-md-3">
				<h4>I<span>nternal</span> M<span>edcine</span></h4>
				<ul>
					<li class="separator"></li>
					<li>General Internal Medicine</li>
					<li>Cardiology</li>
					<li>Oncology</li>
					<li>Nephology</li>
					<li>Neurology</li>
					<li>Psychiatry</li>
					<li>Pulmonology</li>
				</ul>
			</div>
			<div class="col-md-3">
				<h4>S<span>urgery</span></h4>
				<ul>
					<li class="separator"></li>
					<li>General Surgery</li>
					<li>Neurosurgery</li>
					<li>Opthalmology</li>
					<li>Otorhinolaryngology</li>
					<li>Urology</li>
					<li>Anesthesiology</li>
				</ul>
			</div>
			<div class="col-md-3">
				<h4>G<span>eneral</span> P<span>ediatrics</span></h4>
				<ul>
					<li class="separator"></li>
					<li>Cardiology</li>
					<li>Neonatology</li>
					<li>Pulmonology</li>
					<li>Developmental Pediatrics</li>
				</ul>
			</div>
			<div class="col-md-3 specialists-group">
				<h4>R<span>ehabilitation</span> M<span>edicine</span></h4>
				<h4>O<span>bstetrics</span> G<span>ynecology</span></h4>
				<h4>C<span>linical</span> P<span>athology</span></h4>
				<h4>R<span>adiology</span></h4>
			</div>
		</div>
	</div>
</section>
		