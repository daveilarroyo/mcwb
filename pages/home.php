		<section id="intro">
			<video width="100%" autoplay loop>
					  <source src="video/banner.mp4" type="video/mp4">
			</video>
			<div class="container text-center">
				<h2>The <b><i>first</i></b> tertiary hospital <br/> in Western Batangas.</h2>
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<p>
							Medical Center Western Batangas is a modern hospital that 
							can adequately and competently serve the healthcare needs of people
							in the western part of the province of Batangas.
						</p>
					</div>
				</div>
				<button class="btn btn-default">Learn More</button>
				
			</div>
		</section>
		<section class="container">
			<div class="row">
				<div class="col-md-10" id="exterior">
					<img class="img-responsive" src="img/exterior.gif" alt="MCWB Exterior" />
					<p>Located within are doctors’ clinics, diagnostic and treatment facilities, 
					and administrative offices. Parking spaces around the hospital 
					can accommodate more th an 60 vehicles.
					</p>
				</div>
				<div class="col-md-2" id="building-stats">
					<ul>
						<li>
							<span>MODERN</span>
							<div>4</div>
							<span>storey building</span>
						</li>
						<li class="separator"></li>
						<li>
							<div>7,000</div>
							<span>square meters</span>
						</li>
						<li class="separator"></li>
						<li>
							<div>80+</div>
							<span>beds</span>
						</li>
					</ul>
					<button class="btn btn-primary btn-xs">Take a tour</button>
				</div>
			</div>
		</section>
		<section>
			<div class="container text-center" id="team-stats">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<h2>Optimum medical care with the help of our</h2>
					</div>
					<div class="team-info col-md-2 col-md-offset-3">
						<span class="uc">More than</span>
						<div>60</div>
						<span>Specialist Doctors</span>
					</div>
					<div class="plus col-md-2">
					+
					</div>
					<div class="team-info col-md-2">
						<span class="uc">Almost</span>
						<div>200</div>
						<span>Support Staff</span>
					</div>
				</div>
			</div>
			
			<ul id="services">
				<li><img src="img/therapy.gif" alt="Therapy" /></li>
				<li><img src="img/mammogram.gif" alt="Mammogram" /></li>
				<li><img src="img/ct-scan.gif" alt="CT Scan" /></li>
			</ul>
		
		</section>
		<section >
			<div class="container text-center">
				<div class="row" id="equipments">
					<div class="col-md-6 col-md-offset-3">
						<p>Brand-new medical equipment and caring staff allow us to provide services that are at par with bigger hospitals in Manila.</p>
					</div>
				</div>
				<div id="equipments-stats" class="row">
					<div class="col-md-4" >
						<ul class="equipments-list text-right">
							<li>CT SCAN</li>
							<li>MRI</li>
							<li>X-RAY</li>
						</ul>
					</div>
					<div id="equipment-count" class="col-md-4" >
						<div>6+</div>
						<span>brand new<br/></span>
						<span>medical equipments</span>
					</div>
					<div class="col-md-4">
						<ul class="equipments-list text-left">
							<li>MAMMO</li>
							<li>ENDOSCOPY</li>
							<li>X-RAY</li>
						</ul>
					</div>
				</div>
				<div id="dream" class="text-center">
					<p>It is our dream to be one of the best hospitals in the Philippines.</p>
				</div>
			</div>
		</section>