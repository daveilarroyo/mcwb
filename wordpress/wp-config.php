<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_mcwb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm?CG`3 _py^b#9!WSR&p~?$bMV8FV`^H]p<R]ro]bdb#,D#p9ulcqPE4ov>(?tA.');
define('SECURE_AUTH_KEY',  '$c:]M&$i_&v6|%G2?iDWMN=%,EAnam-IGuyS>i4:W GCoI?&gV.Is6V`m5AcglPa');
define('LOGGED_IN_KEY',    'kbty*_NagtK{oz]W>b!K]W6xkzT)^;[^5bI73|snt77 mRuQk6N6>z#?lv1jR2oa');
define('NONCE_KEY',        'H7~s YuD%=!c8YQ1Rx~2]v q3:1Y%KchEaD,JL(}6EpFxNqGXS~f9GwBOT|Et@&N');
define('AUTH_SALT',        'g6R|5$f+m>m|OgG;j:FaSqTXB4YjO oV<h0&;- k47-?:6oda$]|=+ay:6nH7dJM');
define('SECURE_AUTH_SALT', '(@c8M1Apy&L%P&w}yI~; eQ:$|kj15XX~ORP.t4LPPU$FBCf;H7zR<{o_.immmbt');
define('LOGGED_IN_SALT',   'V2UM;[HN?B{pPpux3veFz)#DGo@E73q|OO:;qa>/dCW@SpIdOt3(eaHFWGh!>()G');
define('NONCE_SALT',       'ecwZ(:?UM$dn#F>QOBrD>+|L,^FP_k`l#zL$E3e@qgytjxIorK~>`Q-/@_:L*.Bn');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
